<?php

namespace nechyk\FileFinderBundle\Tests\Command;


use Symfony\Component\Console\Tester\CommandTester,
    Symfony\Bundle\FrameworkBundle\Console\Application,
    Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use nechyk\FileFinderBundle\Command\FindInContentCommand;

/**
 * Class: FindInContentCommandTest
 *
 * @see KernelTestCase
 */
class FindInContentCommandTest extends KernelTestCase
{
    /**
     * Runs a command and returns it output
     */
    public function testExecute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new FindInContentCommand());

        $command = $application->find('find:in-content');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array(
                '--search'      => 'EBook',
                '--dir'         => $kernel->locateResource('@nechykFileFinderBundle/Resources/files'),
                '--insensitive' => true,
            )
        );

        $this->assertRegExp('/1\ file/', $commandTester->getDisplay());

    }
}

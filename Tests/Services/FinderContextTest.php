<?php

namespace nechyk\FileFinderBundle\Tests\Services;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase,
    Symfony\Component\DependencyInjection\ContainerBuilder;

use nechyk\FileFinderBundle\Services\FinderContext;

/**
 * Class: FinderContextTest
 *
 * @see KernelTestCase
 */
class FinderContextTest extends KernelTestCase
{
    /**
     * container
     *
     * @var mixed
     */
    private $container;

    /**
     * testExecute
     */
    public function testExecute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();

        // init search parameters
        $params = [];
        $params[] = [
            'slug' => 'EBook',
            'dir' => $kernel->locateResource('@nechykFileFinderBundle/Resources/files'),
            'insensitive' => false,
            'count' => 0
        ];
        $params[] = [
            'slug' => 'EBook',
            'dir' => $kernel->locateResource('@nechykFileFinderBundle/Resources/files'),
            'insensitive' => true,
            'count' => 1
        ];
        $params[] = [
            'slug' => 'ADVENTURES',
            'dir' => $kernel->locateResource('@nechykFileFinderBundle/Resources/files'),
            'insensitive' => true,
            'count' => 2
        ];

        // check Service working
        foreach ($params as $key => $param) {
            $result = $this->container->get('nechyk_content_finder.finder')
                ->doSearch(
                    $param['slug'],
                    $param['dir'],
                    $param['insensitive']
                )
            ;
            $this->assertEquals($param['count'], count($result));
        }
    }

}
